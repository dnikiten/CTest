const express = require("express");
const app     = express();
const path    = require("path");

const api     = require("./api/api");

const portNum = (process.argv[2]) ? process.argv[2] : "8080";

// Parse URL-encoded bodies (as sent by HTML forms)
app.use(express.urlencoded());

// Parse JSON bodies (as sent by API clients)
app.use(express.json());

//
// API ENDPOINTS
//
app.post('/api/insert_results', async (req, res) => {
    console.log(req.body);
    let results = (req.body) ? req.body : null;
    res.send(await api.insert_results(results));
});
app.post('/api/delete_row', async (req, res) => {
    console.log(req.body);
    let id = (req.body.id) ? req.body.id : null;
    res.send(await api.delete_row(id));
});
app.post('/api/delete_all_rows', async (req, res) => {
    res.send(await api.delete_all_rows());
});
app.get('/api/get_results', async (req, res) => {
    res.send(await api.get_results());
});

// Ctest save results
app.post('/api/save_ctest_results', async (req, res) => {
    res.send(await api.save_ctest_results(req.body));
});

async function getData() {
    let results = $.ajax({
        url: "/api/get_results",
        method: "GET"
    });
    return results;
}
// Catch all other api calls
app.get('/api/:endpoint', (req, res) => {
    console.log("404 Endpoint \"/api/" + req.params.endpoint + "\" not found.");
    res.sendFile(path.join(__dirname + '/static/404.html'))
});


//
// STATIC FILES
//
app.get("/favicon.ico", (req, res) => {
    res.sendFile(path.join(__dirname + '/static/assets/favicon.ico'));
});
app.get("/", (req, res) => {
    // TEMPORARY TAKEOVER
    // res.sendFile(path.join(__dirname + '/static/index.html'));
    res.sendFile(path.join(__dirname + '/static/ctest.html'));
});
app.get("/visual", (req, res) => {
    res.sendFile(path.join(__dirname + '/static/visual.html'));
});
app.get("/ctest", (req, res) => {
    res.sendFile(path.join(__dirname + '/static/ctest.html'));
});
app.get("/index", (req, res) => {
    res.sendFile(path.join(__dirname + '/static/index.html'));
});

app.get("/style.css", (req, res) => {
    res.sendFile(path.join(__dirname + '/static/style/style.css'));
});
app.get("/index.js", (req, res) => {
    res.sendFile(path.join(__dirname + '/static/script/index.js'));
});
app.get("/visualization.js", (req, res) => {
    res.sendFile(path.join(__dirname + '/static/script/visualization.js'));
});
app.get("/plotly.js", (req, res) => {
    res.sendFile(path.join(__dirname + '/static/script/plotly-latest.min.js'));
});
app.get("/p5.js", (req, res) => {
    // res.sendFile(path.join(__dirname + '/static/script/p5/p5.min.js'));
    res.sendFile(path.join(__dirname + '/static/script/p5/p5.js'));
});
app.get("/render.js", (req, res) => {
    res.sendFile(path.join(__dirname + '/static/script/render.js'));
});

app.get("/ctest/:filename", (req, res) => {
    res.sendFile(path.join(__dirname + '/static/script/ctest/dist/' + req.params.filename));
});
app.get("/ctest/resources/:filename", (req, res) => {
    res.sendFile(path.join(__dirname + '/static/script/ctest/resources/' + req.params.filename));
});
app.get("/menu.js", (req, res) => {
    res.sendFile(path.join(__dirname + '/static/script/menu.js'));
});
// app.get("/ctest.js.map", (req, res) => {
//     res.sendFile(path.join(__dirname + '/static/script/ctest/dist/ctest.js'));
// });



app.get('/:endpoint', (req,res) => {
    console.log("404 Endpoint \"/" + req.params.endpoint + "\" not found.");
    res.sendFile(path.join(__dirname + '/static/404.html'))
});

// Replace with environment variable
app.listen(portNum);
console.log(`Hosted at: http://localhost:${portNum}/`);