const sqlite3 = require('sqlite3');
const path   = require("path");
const fs     = require("fs");
const db_filename = "database.db";

// Mode: sqlite3.OPEN_READONLY, sqlite3.OPEN_READWRITE and sqlite3.OPEN_CREATE
function createConnection(mode) {
    let db = new sqlite3.Database(path.join(__dirname, db_filename), mode);
    if (!db) {
        console.error("Error connecting to database");
    }
    return db;
}

exports.insert_results = (results) => new Promise((resolve, reject) => {
    let db = createConnection(sqlite3.OPEN_READWRITE);
    if (db) {
        let t_name = (results.prod) ? 'results_prod' : 'results_test'
        let q_str = `
        INSERT INTO 
            ${t_name} (uuid, ref_L, ref_u, ref_v, res_L, res_u, res_v, distance, label, trial, note)
        VALUES
            ("${results.uuid}", ${results.ref_L}, ${results.ref_u}, ${results.ref_v}, ${results.res_L}, ${results.res_u}, ${results.res_v}, ${results.distance}, "${results.label}", ${results.trial}, "${results.note}");`;

        db.run(q_str, function (err, result) {
            db.close();
            if (err) {
                reject(err);
            } else {
                resolve(result);
            }
        });
    } else {
        reject(db);
    }
});

exports.get_results = () => new Promise((resolve, reject) => {
    let db = createConnection(sqlite3.OPEN_READONLY);

    if (db) {
        let q_str = "SELECT * FROM results_test;";
        db.all(q_str, function (err, result) {
            db.close();
            if (err) {
                reject(err);
            } else {
                resolve(result);
            }
        });
    } else {
        reject(db);
    }
});

exports.delete_row = (id) => new Promise((resolve, reject) => {
    let db = createConnection(sqlite3.OPEN_READWRITE);

    if (db) {
        let q_str = `DELETE FROM results_test WHERE id=${id};`;
        db.run(q_str, function(err, result) {
            db.close();
            if (err) {
                reject(err);
            } else {
                resolve(result);
            }
        });
        
    } else {
        reject(db);
    }
});

exports.delete_all_rows = () => new Promise((resolve, reject) => {
    let db = createConnection(sqlite3.OPEN_READWRITE);

    if (db) {
        let q_str = `DELETE FROM results_test;`;
        db.run(q_str, function(err, result) {
            db.close();
            if (err) {
                reject(err);
            } else {
                resolve(result);
            }
        });
        
    } else {
        reject(db);
    }
});

// This function is what is being used to store entries in the db
exports.save_ctest_results = (results) => new Promise((resolve, reject) => {
    console.log("In");
    let db = createConnection(sqlite3.OPEN_READWRITE);
    if (db) {
        let t_name = (results.prod) ? 'ctest_results_prod' : 'ctest_results_test'

        let q_str = `
        INSERT INTO 
            ${t_name} 
            (uuid, env_id, age, sex, total_trials, correct_trials, skipped_trials, total_time, 
            trial_time, individual_trial_count, individual_correct, individual_skip, individual_times,    
            trial_type, b_red, b_green, b_blue, min_r, min_g, min_b, max_r, max_g, max_b,
            b_L, b_u, b_v, min_L, min_u, min_v, max_L, max_u, max_v, distance)
        VALUES
            ("${results[0]}", ${parseInt(results[1])}, ${parseInt(results[2])}, ${parseInt(results[3])},  
            ${results[4]}, ${results[5]}, ${results[6]}, ${results[7]}, 
            ${parseFloat(results[8])}, ${parseInt(results[9])}, ${parseInt(results[10])}, ${parseInt(results[11])},
            "${results[12]}", "${results[13]}",
            ${parseInt(results[14])}, ${parseInt(results[15])}, ${parseInt(results[16])}, 
            ${parseInt(results[17])}, ${parseInt(results[18])}, ${parseInt(results[19])}, 
            ${parseInt(results[20])}, ${parseInt(results[21])}, ${parseInt(results[22])}, 
            ${parseFloat(results[23])}, ${parseFloat(results[24])}, ${parseFloat(results[25])}, 
            ${parseFloat(results[26])}, ${parseFloat(results[27])}, ${parseFloat(results[28])}, 
            ${parseFloat(results[29])}, ${parseFloat(results[30])}, ${parseFloat(results[31])}, ${parseFloat(results[32])});`;

        console.log(q_str);

        db.run(q_str, function (err, result) {
            db.close();
            if (err) {
                reject(err);
            } else {
                resolve(result);
            }
        });
    } else {
        reject(db);
    }
});