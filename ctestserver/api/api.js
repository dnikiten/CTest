const db = require('../db/db');
const { response } = require('express');

// Function to validate and insert results json
exports.insert_results = async (results) => {
    let res = {
        success: true,
        message: "Data inserted successfully."
    }

    if (validateResults(results)) {
        let insert_res = await db.insert_results(results); 
        if (insert_res) {
            res.success = false;
            res.message = insert_res;
        }
    } else {
        res.success = false;
        res.message = "Invalid results. Nothing inserted.";
    }

    return res;
}


exports.get_results = async () => {
    let results = await db.get_results();
    
    if (results) {
        return results;
    } else {
        return [];
    }
}


exports.delete_row = async (id) => {
    let res = {
        success: true,
        message: "Data deleted successfully."
    }

    let delete_res = await db.delete_row(id); 
    if (delete_res) {
        res.success = false;
        res.message = delete_res;
    }

    return res;
}


exports.delete_all_rows = async () => {
    let res = {
        success: true,
        message: "All data deleted successfully."
    }

    let delete_res = await db.delete_all_rows(); 
    if (delete_res) {
        res.success = false;
        res.message = delete_res;
    }
    console.log(res);
    return res;
}

exports.save_ctest_results = async (data) => {
    let res = {
        success: true,
        message: "Ctest data saved successfully"
    }
    
    for (let line of data) {
        let insert_res = await db.save_ctest_results(line);
        if (insert_res) {
            res.success = false;
            res.message = "Error storing ctest results in database"
        }
    }

    return res;
}


function validateResults(res) {
    return (res.uuid && res.ref_L && res.res_L && res.ref_u && res.res_u && res.ref_v && res.res_v && res.label && res.note)
}