$(document).ready(async () => {
    const raw_data = await getData();
    // Populate data table on load
    if (!raw_data) {
        changeAlert("#alert_container", "danger", "Raw data not retrieved!");
        console.error("Raw data not retrieved!");
    }

    populateDataTable(raw_data);
    populateDataSummary(raw_data);

    // Handle insert button click
    $("#insert_button").click(insert_button_click);
    // Handle randomize button click
    $("#randomize_button").click(randomize_button_click);
    // Handle clear button click
    $("#clear_button").click(clear_button_click);

    // Handle table delete button clicks
    $("table").on("click", "button.delete_button", function(obj){
        if (obj.target.value >= 0) {
            console.log("Deleting id " + obj.target.value);
            deleteRow(obj.target.value);
        } else {
            console.log("Invalid delete id:");
            console.log(obj.target);
        }
        location.reload();
    });

    $("#label_selector").change(function() {
        populateDataSummary(raw_data, $("#label_selector").val());
    });

    // Delete all buttons
    // Hide the confirmation buttons on startup
    $("#delete_confirmation_row").hide();
    $("#delete_all_btn").click(function () {
        $("#delete_confirmation_row").show();
    });
    $("#delete_all_btn_yes").click(/*delete_all_confirm_button_click(true)*/ async function () {
        let result = await deleteAllRows();
        if (result.success) {
            changeAlert("#table_alert_container", "success", result.message);
        } else {
            changeAlert("#table_alert_container", "danger", result.message);
        }
        location.reload();
    });
    $("#delete_all_btn_no").click(function () {
        $("#delete_confirmation_row").hide();
    });
});

async function getData() {
    let results = $.ajax({
        url: "/api/get_results",
        method: "GET"
    });
    return results;
}

// Function to retrieve data and populate table
function populateDataTable(raw_data) {
    resetAlert("#table_alert_container");
    $("#data_table").html(""); // Clear old data

    // Set header
    $("#data_table").html(`
    <thead>
        <tr>
        <th>ID</th>
        <th>UUID</th>
        <th>Reference Luv</th>
        <th>Result Luv</th>
        <th>Distance</th>
        <th>Label</th>
        <th>Trial</th>
        <th>Note</th>
        <th>Delete</th>
        </tr>
    </thead>`);

    let body_str = "<tbody>";
    for(let row of raw_data) {
        // let delete_button = `<button class="btn btn-danger delete_button" onclick="deleteRow(${row.id})">X</button>`;
        let delete_button = `<button class="btn btn-danger delete_button" value="${row.id}">X</button>`;
        body_str += `
        <tr>
            <td>${row.id}</td>
            <td>${row.uuid}</td>
            <td>(${Number((row.ref_L).toFixed(1))}, ${Number((row.ref_u).toFixed(1))}, ${Number((row.ref_v).toFixed(1))})</td>
            <td>(${Number((row.res_L).toFixed(1))}, ${Number((row.res_u).toFixed(1))}, ${Number((row.res_v).toFixed(1))})</td>
            <td>${Number((row.distance).toFixed(3))}</td>
            <td>${row.label}</td>
            <td>${row.trial}</td>
            <td>${row.note}</td>
            <td>${delete_button}</td>
        </tr>`;
    }
    body_str += "</tbody>";
    $("#data_table").append(body_str);

    $("#data_table").append("</tbody>");

    $("#data_table").DataTable();
    $(".dataTables_length").addClass('bs_select');
}

function populateDataSummary(raw_data, selected_label="ALL") {
    // Set label dropdown
    const dropdown = $("#label_selector");
    const avg_dist_text = $("#average_distance_text");

    // Get unique label options
    let options = [];
    for (let row of raw_data) {
        if (!options.includes(row.label)) {
            options.push(row.label);
        }
        if (!options.includes(row.uuid)) {
            options.push(`${row.uuid}`);
        }
    }

    // Populate label dropdown
    dropdown.html(`<option value="ALL" selected>All</option>`);
    for (let cur_opt of options.sort()) {
        dropdown.append(`<option value="${cur_opt}">${cur_opt}</option>`);
    }

    // Calculate data summary points
    distance_vals = [];
    for (let row of raw_data) {
        if (selected_label == "ALL" || selected_label == row.label || selected_label == `${row.uuid}`) {
            distance_vals.push(row.distance);
        }
    }

    if (distance_vals.length > 0) {
        const average_dist = distance_vals.reduce((total, num) => { return total + num}) / distance_vals.length;
        avg_dist_text.html(`Average Distance [${selected_label}]: ${average_dist.toFixed(3)}`)
    }
    else {
        avg_dist_text.html(`No data.`);
    }

}

// Input handling functions
function insert_button_click() {
    let valid = true;
    const results = {
        uuid: $("#result_uuid").val(),
        ref_L: $("#result_ref_L").val(),
        res_L: $("#result_res_L").val(),
        ref_u: $("#result_ref_u").val(),
        res_u: $("#result_res_u").val(),
        ref_v: $("#result_ref_v").val(),
        res_v: $("#result_res_v").val(),
        distance: $("#result_distance").val(),
        label: $("#result_label").val(),
        trial: parseInt($("#result_trial").val()),
        note: $("#result_note").val()
    }

    for(let key in results) {
        if (key != "note" && results[key] == "") {
            valid = false;
            changeAlert("#alert_container", "danger", "All values but 'note' are required.");
        } 
        
        if (key == "trial" && !Number.isInteger(results[key])) {
            valid = false;
            changeAlert("#alert_container", "danger", "Trial must be an integer.");
        }
    }
    
    if (valid) {
        $.post("/api/insert_results", results, function(data, status) {
            console.log(data);
            let alert_type = (data.success) ? "success" : "danger";
            changeAlert("#alert_container", alert_type, data.message);
            location.reload();
        });
        
        // .fail(function(jqXHR, textStatus, errorThrown) {
        //     console.log("Failed: " + errorThrown);
        //     changeAlert("danger", errorThrown);
        // });
    }
}

function randomize_button_click() {
    const L_val_range = [0, 100];
    const u_val_range = [-134, 220];
    const v_val_range = [-140, 122];

    let results = {
        uuid: uuidv4(),
        ref_L: randFloat(L_val_range),
        res_L: randFloat(L_val_range),
        ref_u: randFloat(u_val_range),
        res_u: randFloat(u_val_range),
        ref_v: randFloat(v_val_range),
        res_v: randFloat(v_val_range),
        label: "LabelName",
        trial: 1,
        note: "Randomized values"
    }

    results.distance = calculateEuclideanDistance(results);


    $("#result_uuid").val(results.uuid);
    $("#result_ref_L").val(results.ref_L);
    $("#result_res_L").val(results.res_L);
    $("#result_ref_u").val(results.ref_u);
    $("#result_res_u").val(results.res_u);
    $("#result_ref_v").val(results.ref_v);
    $("#result_res_v").val(results.res_v);
    $("#result_distance").val(results.distance);
    $("#result_label").val(results.label);
    $("#result_trial").val(results.trial);
    $("#result_note").val(results.note);

    changeAlert("#alert_container", "info", "Values randomized.");
}

function clear_button_click() {
    $("#result_uuid").val("");
    $("#result_ref_L").val("");
    $("#result_res_L").val("");
    $("#result_ref_u").val("");
    $("#result_res_u").val("");
    $("#result_ref_v").val("");
    $("#result_res_v").val("");
    $("#result_distance").val("");
    $("#result_label").val("");
    $("#result_trial").val("");
    $("#result_note").val("");

    changeAlert("#alert_container", "info", "Values cleared.");
}

// Function to generate test uuids DO NOT USE IN PRODUCTION
function uuidv4() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
      return v.toString(16);
    });
}
// Function to generate float in range
function randFloat(range) {
    return (Math.random() * (range[1] - range[0]) + range[0]).toFixed(3);
}

function resetAlert(id) {
    $(id).html("");
}
// Function to change the insert data alert
function changeAlert(id, type, str) {
    $(id).html(`
    <div class="alert alert-${type}" role="alert">
        ${str}
    </div>
    `);
}

function deleteRow(id) {
    console.log("Deleting id: " + id);
    $.post("/api/delete_row", {'id': id}, function(data, status) {
        if (data.success) {
            changeAlert("#table_alert_container", "success", data.message);
        } else {
            changeAlert("#table_alert_container", "danger", data.message);
        }

    });
}

async function deleteAllRows() {
    let data = $.post("/api/delete_all_rows");
    return data;
}

function calculateEuclideanDistance(results) {
    let a = [results.ref_L, results.ref_u, results.ref_v];
    let b = [results.res_L, results.res_u, results.res_v];

    let step = Math.pow((a[0] - b[0]), 2) + Math.pow((a[1] - b[1]), 2) + Math.pow((a[2] - b[2]), 2);
    let euc_dist = Math.sqrt(step);

    return euc_dist.toFixed(5);
}

// Results validation function
function validateResults(results) {
    return true;
}