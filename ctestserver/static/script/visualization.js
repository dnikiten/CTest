
async function getData() {
    let results = $.ajax({
        url: "/api/get_results",
        method: "GET"
    });
    return results;
}

function getColourFromLabel(str) {
    let parts = str.split(" ");
    let r = parseFloat(parts[0]);
    let g = parseFloat(parts[1]);
    let b = parseFloat(parts[2]);

    return Math.floor(r), Math.floor(g), Math.floor(b)
}

function generate3DPlot(data) {
    function unpack(rows, key) {
        return rows.map(function(row) { return row[key]; });
    }

    console.log(data);

    var chart_data = [
        // {
        // x: unpack(data, 'ref_u'),
        // y: unpack(data, 'ref_v'),
        // z: unpack(data, 'ref_L'),
        // mode: 'markers',
        // type: 'scatter3d',
        // marker: {
        // color: 'rgb(23, 190, 207)',
        // size: 2
        // }
    // },
    // {
    //     alphahull: 7,
    //     opacity: 0.1,
    //     type: 'mesh3d',
    //     x: unpack(data, 'x'),
    //     y: unpack(data, 'y'),
    //     z: unpack(data, 'z')
    // }
    ];

    for (let row of data) {
        let r_val, g_val, b_val = getColourFromLabel(row.label);
        chart_data.push({
            x: [row.ref_u, row.res_u],
            y: [row.ref_v, row.res_v],
            z: [row.ref_L, row.res_L],
            text: ["Ref", `Res ${row.note}(${row.trial})`],
            mode: 'markers',
            type: 'scatter3d',
            marker: {
                color: `rgb(${r_val}, ${g_val}, ${b_val})`,
                size: 3
            }
        });
        
    }
    

    var layout = {
        autosize: true,
        height: 600,
        scene: {
            aspectratio: {
                x: 1,
                y: 1,
                z: 1
            },
            camera: {
                center: {
                    x: 0,
                    y: 0,
                    z: 0
                },
                eye: {
                    x: 1.25,
                    y: 1.25,
                    z: 1.25
                },
                up: {
                    x: 0,
                    y: 0,
                    z: 1
                }
            },
            xaxis: {
                type: 'linear',
                zeroline: false
            },
            yaxis: {
                type: 'linear',
                zeroline: false
            },
            zaxis: {
                type: 'linear',
                zeroline: false
            }
        },
        title: '3d point clustering',
        width: 600
    };

    Plotly.newPlot('frame', chart_data, layout);
}

$(document).ready(async () => {
    const raw_data = await getData();
    // Populate data table on load
    if (!raw_data) {
        changeAlert("#alert_container", "danger", "Raw data not retrieved!");
        console.error("Raw data not retrieved!");
    }

    generate3DPlot(raw_data);
});

