// Note (from Thomas) - As far as I can tell this file is no longer used, instead the noise is
// generated in the calibration trial file

import { ColourConverterStandalone } from './ColourConverterStandalone.js';

class LuminanceNoiseGenerator {
    static generateLuminanceNoise(rgb: number, lum_noise: number[], cap_lum: boolean, cap_rgb: boolean, use_extreme_rgb: boolean): number[] {
        let result: number[] = Array.apply(null, Array(lum_noise.length)).map(function(){});
        let LUV: number[]   = ColourConverterStandalone.RGBtoLUV(rgb);

        for(let n=0; n<lum_noise.length; n++) {
            let noise: number = lum_noise[n];

            // Check to see if luminance exceeds the maximum possible
            if (LUV[0] + noise > ColourConverterStandalone.MAX_LUM_LUV) {
                if (cap_lum) {
                    noise = ColourConverterStandalone.MAX_LUM_LUV - LUV[0];
                }
                else {
                    return null;
                }
            }

            // Check to see if luminance falls below minimum possible
            if (LUV[0] + noise < ColourConverterStandalone.MIN_LUM_LUV) {
                if (cap_lum) {
                    noise = -1.0 * (LUV[0] - ColourConverterStandalone.MIN_LUM_LUV);
                }
                else {
                    return null;
                }
            }

            // Generate the LUV color with modified luminance and convert it to RGB
            let LUV2: number[] = [LUV[0] + noise, LUV[1], LUV[2]];
            let RGB2: number[] = ColourConverterStandalone.unpackageIntRGB(ColourConverterStandalone.LUVtoRGB(LUV2));

            let rgb_min: number = (use_extreme_rgb) ? 0 : 1;
            let rgb_max: number = (use_extreme_rgb) ? 255 : 254;
            
            if (RGB2[0] < rgb_min || RGB2[1] < rgb_min || RGB2[2] < rgb_min) {
                if (cap_rgb) {
                    RGB2[0] = Math.max(RGB2[0], rgb_min);
                    RGB2[1] = Math.max(RGB2[1], rgb_min);
                    RGB2[2] = Math.max(RGB2[2], rgb_min);
                }
                else {
                    return null;
                }
            }

            if (RGB2[0] > rgb_max || RGB2[1] > rgb_max || RGB2[2] > rgb_max) {
                if (cap_rgb) {
                    RGB2[0] = Math.min(RGB2[0], rgb_max);
                    RGB2[1] = Math.min(RGB2[1], rgb_max);
                    RGB2[2] = Math.min(RGB2[2], rgb_max);
                }
                else {
                    return null;
                }
            }

            result[n] = 0xFF000000 | ColourConverterStandalone.packageIntRGB(RGB2[0], RGB2[1], RGB2[2]);
        }
        
        return result;
    }
}

export { LuminanceNoiseGenerator }