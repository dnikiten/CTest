var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
import { CalibrationConstants } from './CalibrationConstants.js';
import { ColourConverterStandalone } from './ColourConverterStandalone.js';
import { CalibrationTrial } from './CalibrationTrial.js';
import { CalibrationType } from './CalibrationType.js';
class CTest {
    constructor(num_rep) {
        this.num_repetitions = num_rep;
        this.testing_trials = [];
        this.finished_trials = [];
        this.mask_index = 0;
        this.trial_index = 0;
        this.progressing = false;
        this.start_time = new Date().getTime();
        this.menu_time = 0;
        this.UUID = "";
        this.env_id = "";
        this.age = "";
        this.sex = "";
        this.time_record = Date.now();
        // Need progress bar?
        // Need background masks java: 
        // BufferedImage[] backgroundMasks = new BufferedImage[8];
        this.background_masks = [];
        this.background_masks_bool = [];
        // Random index in range 0 -> this.background_masks.length
        // this.mask_index      = 
        this.screen_count = 0;
        this.skip_count = 0;
        this.correct_count = 0;
        this.estimated_screen_count = 0.0;
        this.settings = {
            size: [400, 400]
        };
        this.is_complete = false;
    }
    setup() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                yield this.generateCalibrationTrials();
            }
            catch (err) {
                // Handle and safely exit
                console.log(err);
            }
            // for(let i=0; i < 8; i++) {
            // }
            console.log("setting up");
            const canvas = document.getElementById('mask_preload');
            canvas.width = 400;
            canvas.height = 400;
            const ctx = canvas.getContext('2d');
            // let yval=400;
            for (let key in CalibrationConstants.MASKS) {
                let img = document.getElementById(`mask_${key}`);
                ctx.drawImage(img, 0, 0, 400, 400);
                let img_data = ctx.getImageData(0, 0, 400, 400);
                // This isn't working because ImageData clamps down all the pixel values into a 1D array
                // E.g. [pixel0_r, pixel0_g, pixel0_b, pixel0_a, pixel1_r, pixel1_g, ....]
                this.background_masks[CalibrationConstants.MASKS[key]] = img_data;
                // yval = yval + 400;
            }
            // let vals: Array<number> = Array();
            // let counts: Array<number> = Array();
            // let inds: Array<number[]> = Array();
            // console.log("Checking...");
            // for(let cur of this.background_masks) {
            //     for(let val of cur.data) {
            //         if (vals.indexOf(val) < 0) {
            //             vals.push(val);
            //             counts.push(0);
            //         }
            //         else {
            //             counts[vals.indexOf(val)]++;
            //         }
            //     }
            // }
            let masks_d = [];
            for (let cur_m = 0; cur_m < this.background_masks.length; cur_m++) {
                let cur = this.background_masks[cur_m];
                let cur_m_bools = [];
                for (let i = 0; i < cur.data.length - 3; i = i + 4) {
                    let srgb = [cur.data[i], cur.data[i + 1], cur.data[i + 2], cur.data[i + 3]];
                    let is_white = true;
                    for (let v of srgb) {
                        if (v != 255) {
                            is_white = false;
                            break;
                        }
                    }
                    if (is_white) {
                        cur_m_bools.push(1);
                    }
                    else {
                        cur_m_bools.push(0);
                    }
                }
                // console.log(cur_m_bools);
                masks_d.push(cur_m_bools);
            }
            this.background_masks_bool = masks_d;
            console.log("Done");
            // console.log(masks_d);
            // for(let m of masks_d) {
            //     console.log("MASK:");
            //     for(let i = 0; i < m.length - 400; i=i+400) {
            //         // console.log(m[i].slice(i, i + 400));
            //     }
            // }
            this.progressing = false;
            return new Promise(function (resolve, reject) { resolve(); });
        });
    }
    isProgressing() {
        return this.progressing;
    }
    isComplete() {
        return this.is_complete;
    }
    nextTrial(time) {
        this.progressing = true;
        this.time_record = time;
        this.mask_index = Math.floor(Math.random() * this.background_masks.length);
    }
    getCurrentTrialNoise() {
        let cur_trial = this.getCurrentTrial();
        let bg_noise = cur_trial.getFullWalkingColors(0);
        let fg_noise = cur_trial.getFullWalkingColors(cur_trial.getCurrentIndex());
        return [bg_noise, fg_noise];
    }
    getCurrentTrial() {
        let cur_trial = this.testing_trials[this.trial_index];
        return cur_trial;
    }
    getBGMaskDataBool(x, y) {
        let ind = (y * 400) + x;
        return this.background_masks_bool[this.mask_index][ind];
    }
    getBGMaskData(x, y) {
        // console.log(`${this.mask_index} ${x} ${y}`);
        // console.log(this.background_masks[this.mask_index]);
        let ind = ((y * this.background_masks[this.mask_index].width) + x) * 4;
        let rgb = ColourConverterStandalone.packageIntRGB(this.background_masks[this.mask_index].data[ind], this.background_masks[this.mask_index].data[ind + 1], this.background_masks[this.mask_index].data[ind + 2]);
        // console.log(`y(${y}) * width(${this.background_masks[this.mask_index].width}) + x(${x}) = ind(${ind}) -> ${this.background_masks[this.mask_index].data[ind]}`);
        // console.log(`ind:${ind} -> ${rgb}`);
        return rgb;
    }
    getMaskIndex() {
        return this.mask_index;
    }
    incrementSkipCount() {
        this.skip_count++;
    }
    incrementCorrectCount() {
        this.correct_count++;
    }
    incrementScreenCount() {
        this.screen_count++;
    }
    finishATrial(cur_trial) {
        this.finished_trials.push(cur_trial);
        this.testing_trials.splice(this.trial_index, 1);
        if (this.testing_trials.length == 0) {
            let test_data = [];
            let total_time = new Date().getTime() - this.start_time - this.menu_time;
            let uuid = this.UUID;
            let env_id = this.env_id;
            let age = this.age;
            let sex = this.sex;
            // console.log(`UUID: TODO\tTotal Trials: ${this.screen_count}\tCorrect: ${this.correct_count}\tSkipped: ${this.skip_count}\tTotal Time: ${total_time}`);
            // console.log("TYPE\tB_RED\tB_GREEN\tB_BLUE\tMIN_R\tMIN_G\tMIN_B\tMAX_R\tMAX_G\tMAX_B\tB_L\tB_U\tB_V\tMIN_L\tMIN_U\tMIN_V\tMAX_L\tMAX_U\tMAX_V\tDISTANCE");
            for (let printCT of this.finished_trials) {
                let ct_str = printCT.toString();
                let ct_parts = ct_str.split('\t');
                let cur_data = [uuid, env_id, age, sex, this.screen_count, this.correct_count, this.skip_count, total_time,
                    printCT.getTotalTrialTime(), printCT.getScreenCount(), printCT.getCorrectCount(),
                    printCT.getSkipCount(), printCT.getIndividualTimesAsString()].concat(ct_parts);
                console.log(cur_data);
                test_data.push(cur_data);
            }
            this.is_complete = true;
            this.saveData(test_data).then((resp) => {
                console.log(resp);
            });
        }
    }
    onToNext() {
        this.trial_index++;
        if (this.trial_index >= this.testing_trials.length) {
            shuffle(this.testing_trials);
            this.trial_index = 0;
        }
        this.time_record = Date.now();
    }
    saveData(data) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield fetch('/api/save_ctest_results', {
                method: 'POST',
                mode: 'cors',
                credentials: 'same-origin',
                headers: {
                    'Content-Type': 'application/json'
                    // 'Content-Type': 'application/x-www-form-urlencoded',
                },
                redirect: 'follow',
                referrerPolicy: 'no-referrer',
                body: JSON.stringify(data) // body data type must match "Content-Type" header
            });
            return response.json();
        });
    }
    loadConfusionLineColors() {
        return __awaiter(this, void 0, void 0, function* () {
            // Define return value to size NUM_CONFUSION_LINE_COLORS
            // This defines AND initializes all values to null
            let confusion_line_colors = Array.apply(null, Array(CalibrationConstants.NUM_CONFUSION_LINES)).map(function () { });
            const file_contents = yield fetch(CalibrationConstants.RESOURCE_PATH + 'colorsNoNoise.txt').then(response => response.text());
            const lines = file_contents.split('\n');
            for (let i = 0; i < CalibrationConstants.NUM_CONFUSION_LINES; i++) {
                const line_split = lines[i].split('\t');
                let line_colors = Array.apply(null, Array(+line_split[0])).map(function () { });
                for (let j = 0; j < +line_split[0]; j++) {
                    line_colors[j] = +line_split[j + 1];
                }
                confusion_line_colors[i] = line_colors;
            }
            return confusion_line_colors;
        });
    }
    generateCalibrationTrials() {
        return __awaiter(this, void 0, void 0, function* () {
            let confusion_line_colors = yield this.loadConfusionLineColors();
            let bases = [
                { c: ColourConverterStandalone.packageIntRGB(17, 17, 17), dir: [CalibrationType.LUMINANCE_UP] },
                { c: ColourConverterStandalone.packageIntRGB(48, 48, 48), dir: [CalibrationType.LUMINANCE_UP, CalibrationType.LUMINANCE_DOWN] },
                { c: ColourConverterStandalone.packageIntRGB(82, 82, 82), dir: [CalibrationType.LUMINANCE_UP, CalibrationType.LUMINANCE_DOWN] },
                { c: ColourConverterStandalone.packageIntRGB(119, 119, 119), dir: [CalibrationType.LUMINANCE_UP, CalibrationType.LUMINANCE_DOWN] },
                { c: ColourConverterStandalone.packageIntRGB(158, 158, 158), dir: [CalibrationType.LUMINANCE_UP, CalibrationType.LUMINANCE_DOWN] },
                { c: ColourConverterStandalone.packageIntRGB(198, 198, 198), dir: [CalibrationType.LUMINANCE_UP, CalibrationType.LUMINANCE_DOWN] },
                { c: ColourConverterStandalone.packageIntRGB(241, 241, 241), dir: [CalibrationType.LUMINANCE_DOWN] }, // LUM DOWN ONLY
            ];
            console.log(bases);
            for (let i = 0; i < bases.length; i++) {
                for (let j = 0; j < this.num_repetitions; j++) { // Hardcoded 2 num_repititions
                    // Confusion line trials
                    // this.testing_trials.push(new CalibrationTrial(CalibrationType.PROTAN_TOWARD, bases[i], confusion_line_colors));
                    // this.testing_trials.push(new CalibrationTrial(CalibrationType.PROTAN_AWAY, bases[i], confusion_line_colors));
                    // this.testing_trials.push(new CalibrationTrial(CalibrationType.DEUTAN_TOWARD, bases[i], confusion_line_colors));
                    // this.testing_trials.push(new CalibrationTrial(CalibrationType.DEUTAN_AWAY, bases[i], confusion_line_colors));
                    // this.testing_trials.push(new CalibrationTrial(CalibrationType.TRITAN_TOWARD, bases[i], confusion_line_colors));
                    // this.testing_trials.push(new CalibrationTrial(CalibrationType.TRITAN_AWAY, bases[i], confusion_line_colors));
                    for (let d of bases[i].dir) {
                        this.testing_trials.push(new CalibrationTrial(d, bases[i].c, confusion_line_colors));
                    }
                    // Luminance trials
                    // this.testing_trials.push(new CalibrationTrial(CalibrationType.LUMINANCE_DOWN, bases[i].c, confusion_line_colors));
                }
            }
            shuffle(this.testing_trials);
            this.estimated_screen_count = 8.0 * this.testing_trials.length;
        });
    }
    pauseProgression() {
        this.progressing = false;
    }
}
function shuffle(a) {
    for (let i = a.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [a[i], a[j]] = [a[j], a[i]];
    }
    return a;
}
window.CTest = CTest;
export default { CTest };
//# sourceMappingURL=ctest.js.map