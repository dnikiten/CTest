import { ColourConverterStandalone } from './ColourConverterStandalone.js';
import { CalibrationType } from './CalibrationType.js';
import { LuminanceNoiseGenerator } from './LuminanceNoiseGenerator.js';
import { CalibrationConstants } from './CalibrationConstants.js';
class CalibrationTrial {
    constructor(ct, base_color, conf_line_colors) {
        this.calibration_type = ct;
        this.base_color_channels = ColourConverterStandalone.unpackageIntRGB(base_color);
        this.full_walking_colors = [];
        // Timings and individual trial stats
        this.trial_times = [];
        this.screen_count = 0;
        this.skip_count = 0;
        this.correct_count = 0;
        let walking_colors;
        switch (ct) {
            case CalibrationType.LUMINANCE_UP:
                walking_colors = Array.apply(null, Array(256 - Math.max(this.base_color_channels[0], Math.max(this.base_color_channels[1], this.base_color_channels[2]))).map(function () { }));
                for (let i = 0; i < walking_colors.length; i++) {
                    walking_colors[i] = ColourConverterStandalone.packageIntRGB(this.base_color_channels[0] + i, this.base_color_channels[1] + i, this.base_color_channels[2] + i);
                }
                this.populateFullWalkingColors(walking_colors);
                break;
            case CalibrationType.LUMINANCE_DOWN:
                walking_colors = Array.apply(null, Array(1 + Math.min(this.base_color_channels[0], Math.min(this.base_color_channels[1], this.base_color_channels[2]))).map(function () { }));
                for (let i = 0; i < walking_colors.length; i++) {
                    walking_colors[i] = ColourConverterStandalone.packageIntRGB(this.base_color_channels[0] - i, this.base_color_channels[1] - i, this.base_color_channels[2] - i);
                }
                this.populateFullWalkingColors(walking_colors);
                break;
            default:
                console.log('default');
                break;
        }
        this.min_index = 0;
        this.max_index = this.full_walking_colors.length - 1;
    }
    toString() {
        let fwc_ind = Math.floor(CalibrationConstants.RLM_VALUES.length / 2);
        let base_color_rgb = this.full_walking_colors[0][fwc_ind];
        // let base_color_rgb: number[] = ColourConverterStandalone.unpackageIntRGB(base_color);
        let min_color_rgb = this.full_walking_colors[this.min_index][fwc_ind];
        // let min_color_rgb: number[] = ColourConverterStandalone.unpackageIntRGB(min_color);
        let max_color_rgb = this.full_walking_colors[this.max_index][fwc_ind];
        // let max_color_rgb: number[] = ColourConverterStandalone.unpackageIntRGB(max_color);
        let base_luv = ColourConverterStandalone.RGBtoLUV(ColourConverterStandalone.packageIntRGB(base_color_rgb[0], base_color_rgb[1], base_color_rgb[2]));
        let min_luv = ColourConverterStandalone.RGBtoLUV(ColourConverterStandalone.packageIntRGB(min_color_rgb[0], min_color_rgb[1], min_color_rgb[2]));
        let max_luv = ColourConverterStandalone.RGBtoLUV(ColourConverterStandalone.packageIntRGB(max_color_rgb[0], max_color_rgb[1], max_color_rgb[2]));
        let distance = 0.0;
        switch (this.calibration_type) {
            case CalibrationType.LUMINANCE_UP:
            case CalibrationType.LUMINANCE_DOWN:
                let avg_limit_lum = (min_luv[0] + max_luv[0]) / 2.0;
                distance = Math.abs(base_luv[0] - avg_limit_lum);
                break;
        }
        // console.log(base_color);
        // console.log(min_color);
        // console.log(max_color);
        // console.log(base_luv);
        // console.log(distance);
        let str = `${this.getCalibrationTypeName()}\t${base_color_rgb.join("\t")}\t${min_color_rgb.join("\t")}\t${max_color_rgb.join("\t")}\t${base_luv.join("\t")}\t${min_luv.join("\t")}\t${max_luv.join("\t")}\t${distance}`;
        return str;
    }
    getCalibrationTypeName() {
        let str = "";
        switch (this.calibration_type) {
            case CalibrationType.LUMINANCE_UP:
                str = "LUMINANCE_UP";
                break;
            case CalibrationType.LUMINANCE_DOWN:
                str = "LUMINANCE_DOWN";
                break;
        }
        return str;
    }
    getCurrentIndex() {
        if (this.isFirstPresentation()) {
            return this.max_index;
        }
        else {
            return Math.floor((this.max_index + this.min_index) / 2);
        }
    }
    isFirstPresentation() {
        return (this.min_index == 0 && this.max_index == this.full_walking_colors.length - 1);
    }
    isFinished() {
        return (this.min_index + 1) >= this.max_index;
    }
    recordIsDifferentiable(is_diff) {
        if (this.isFirstPresentation()) {
            if (is_diff) {
                this.max_index = this.max_index - 1;
            }
            else {
                this.min_index = this.max_index;
            }
        }
        else {
            if (is_diff) {
                this.max_index = this.getCurrentIndex();
            }
            else {
                this.min_index = this.getCurrentIndex();
            }
        }
    }
    getFullWalkingColors(ind) {
        return this.full_walking_colors[ind];
    }
    populateFullWalkingColors(wc) {
        for (let center of wc) {
            let lum_noise = LuminanceNoiseGenerator.generateLuminanceNoise(center, CalibrationConstants.RLM_VALUES, true, true, true);
            if (lum_noise != null) {
                let lum_noise_int_arr = Array.apply(null, Array(lum_noise.length)).map(function () { });
                for (let i = 0; i < lum_noise.length; i++) {
                    lum_noise_int_arr[i] = ColourConverterStandalone.unpackageIntRGB(lum_noise[i]);
                }
                // console.log(lum_noise_int_arr);
                this.full_walking_colors.push(lum_noise_int_arr);
            }
        }
        // console.log(this.full_walking_colors);
    }
    incrementSkipCount() {
        this.skip_count++;
    }
    incrementCorrectCount() {
        this.correct_count++;
    }
    incrementScreenCount() {
        this.screen_count++;
    }
    getTotalTrialTime() {
        return this.trial_times.reduce((partial_sum, time) => partial_sum + time, 0);
    }
    getScreenCount() {
        return this.screen_count;
    }
    getCorrectCount() {
        return this.correct_count;
    }
    getSkipCount() {
        return this.skip_count;
    }
    getIndividualTimesAsString() {
        return this.trial_times.reduce((partial_string, time) => partial_string + String(time) + ";", "");
    }
}
export { CalibrationTrial };
//# sourceMappingURL=CalibrationTrial.js.map