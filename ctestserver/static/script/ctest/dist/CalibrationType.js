var CalibrationType;
(function (CalibrationType) {
    CalibrationType[CalibrationType["PROTAN_TOWARD"] = 0] = "PROTAN_TOWARD";
    CalibrationType[CalibrationType["PROTAN_AWAY"] = 1] = "PROTAN_AWAY";
    CalibrationType[CalibrationType["DEUTAN_TOWARD"] = 2] = "DEUTAN_TOWARD";
    CalibrationType[CalibrationType["DEUTAN_AWAY"] = 3] = "DEUTAN_AWAY";
    CalibrationType[CalibrationType["TRITAN_TOWARD"] = 4] = "TRITAN_TOWARD";
    CalibrationType[CalibrationType["TRITAN_AWAY"] = 5] = "TRITAN_AWAY";
    CalibrationType[CalibrationType["LUMINANCE_UP"] = 6] = "LUMINANCE_UP";
    CalibrationType[CalibrationType["LUMINANCE_DOWN"] = 7] = "LUMINANCE_DOWN";
})(CalibrationType || (CalibrationType = {}));
export { CalibrationType };
//# sourceMappingURL=CalibrationType.js.map