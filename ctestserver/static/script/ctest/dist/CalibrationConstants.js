class CalibrationConstants {
}
CalibrationConstants.WAIT_TIME = 500;
CalibrationConstants.SIZE = 4;
CalibrationConstants.GAP = 4;
CalibrationConstants.RLM_VALUES = [-5.0, -4.0, -3.0, -2.0, -1.0, 0.0, 1.0, 2.0, 3.0, 4.0, 5.0];
CalibrationConstants.NUM_CONFUSION_LINES = 6;
CalibrationConstants.MASKS = {
    TT: 0,
    TR: 1,
    RR: 2,
    BR: 3,
    BB: 4,
    BL: 5,
    LL: 6,
    TL: 7
};
CalibrationConstants.RESOURCE_PATH = "ctest/resources/";
export { CalibrationConstants };
//# sourceMappingURL=CalibrationConstants.js.map