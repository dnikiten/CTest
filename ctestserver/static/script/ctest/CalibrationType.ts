enum CalibrationType {
    PROTAN_TOWARD = 0,
    PROTAN_AWAY = 1,
	DEUTAN_TOWARD = 2,
	DEUTAN_AWAY = 3,
	TRITAN_TOWARD = 4,
	TRITAN_AWAY = 5,
	LUMINANCE_UP = 6,
	LUMINANCE_DOWN = 7
}

export { CalibrationType };
