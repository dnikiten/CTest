import { CalibrationConstants } from './CalibrationConstants.js';
import { ColourConverterStandalone } from './ColourConverterStandalone.js';
import { CalibrationTrial } from './CalibrationTrial.js';
import { CalibrationType } from './CalibrationType.js';

interface CTest {
    num_repetitions: number;
    testing_trials: CalibrationTrial[];
    finished_trials: CalibrationTrial[];
    mask_index: number;
    trial_index: number;
    progressing: boolean;
    start_time: number;
    menu_time: number;
    UUID: string;
    env_id: string;
    age: string;
    sex: string;
    time_record: Number;
    background_masks: any[];
    background_masks_bool: any[];
    screen_count: number;
    skip_count: number;
    correct_count: number;
    estimated_screen_count: number;
    settings: {};
    is_complete: boolean;
}

class CTest {
    constructor(num_rep: number) {
        this.num_repetitions = num_rep;
        this.testing_trials  = [];
        this.finished_trials = [];
        this.mask_index      = 0;
        this.trial_index     = 0;
        this.progressing     = false;
        this.start_time      = new Date().getTime();
        this.menu_time       = 0;
        this.UUID            = "";
        this.env_id          = "";
        this.age             = "";
        this.sex             = "";
        this.time_record     = Date.now();
        // Need progress bar?
        
        // Need background masks java: 
        // BufferedImage[] backgroundMasks = new BufferedImage[8];
        this.background_masks = [];
        this.background_masks_bool = [];
        
        // Random index in range 0 -> this.background_masks.length
        // this.mask_index      = 

        this.screen_count = 0;
        this.skip_count = 0;
        this.correct_count = 0;
        this.estimated_screen_count = 0.0;
        this.settings = {
            size: [400, 400]
        };
        this.is_complete = false;
    }
    
    
    async setup(): Promise<void> {
        try {
            await this.generateCalibrationTrials();
        }
        catch (err) {
            // Handle and safely exit
            console.log(err);
        }
    
        // for(let i=0; i < 8; i++) {
    
        // }
        console.log("setting up")
        const canvas = <HTMLCanvasElement> document.getElementById('mask_preload');
        canvas.width = 400;
        canvas.height = 400;
        const ctx = canvas.getContext('2d');
        // let yval=400;
        for(let key in CalibrationConstants.MASKS) {
            let img: HTMLImageElement = <HTMLImageElement> document.getElementById(`mask_${key}`);
            
            ctx.drawImage(img, 0, 0, 400, 400);
            let img_data: ImageData = ctx.getImageData(0, 0, 400, 400);
            // This isn't working because ImageData clamps down all the pixel values into a 1D array
            // E.g. [pixel0_r, pixel0_g, pixel0_b, pixel0_a, pixel1_r, pixel1_g, ....]
    
            this.background_masks[CalibrationConstants.MASKS[key]] = img_data; 
            // yval = yval + 400;
        }

        // let vals: Array<number> = Array();
        // let counts: Array<number> = Array();
        // let inds: Array<number[]> = Array();
        // console.log("Checking...");

        // for(let cur of this.background_masks) {
        //     for(let val of cur.data) {
        //         if (vals.indexOf(val) < 0) {
        //             vals.push(val);
        //             counts.push(0);
        //         }
        //         else {
        //             counts[vals.indexOf(val)]++;
        //         }
        //     }
        // }

        
        let masks_d = [];
        for(let cur_m = 0; cur_m < this.background_masks.length; cur_m++) {
            let cur = this.background_masks[cur_m];
            let cur_m_bools = [];
            for (let i = 0; i < cur.data.length - 3; i=i+4) {
                let srgb = [ cur.data[i], cur.data[i+1], cur.data[i+2], cur.data[i+3] ];
                let is_white = true;
                for(let v of srgb) {
                    if (v != 255) {
                        is_white = false;
                        break;
                    }
                }

                if (is_white) {
                    cur_m_bools.push(1);
                }
                else {
                    cur_m_bools.push(0);
                }
            }
            // console.log(cur_m_bools);
            masks_d.push(cur_m_bools);
        }

        this.background_masks_bool = masks_d;

        console.log("Done");
        // console.log(masks_d);
        // for(let m of masks_d) {
        //     console.log("MASK:");
        //     for(let i = 0; i < m.length - 400; i=i+400) {
        //         // console.log(m[i].slice(i, i + 400));
        //     }
        // }
    
        this.progressing = false;

        return new Promise( function(resolve, reject) {resolve()});
    }
    
    isProgressing(): boolean {
        return this.progressing;
    }

    isComplete(): boolean {
        return this.is_complete;
    }

    nextTrial(time: Number): void {
        this.progressing = true;
        this.time_record = time;
        this.mask_index = Math.floor(Math.random() * this.background_masks.length);
    }
    
    getCurrentTrialNoise(): [number[][], number[][]] {
        let cur_trial: CalibrationTrial = this.getCurrentTrial();
        let bg_noise: number[][] = cur_trial.getFullWalkingColors(0);
        let fg_noise: number[][] = cur_trial.getFullWalkingColors(cur_trial.getCurrentIndex());

        return [bg_noise, fg_noise];
    }

    getCurrentTrial(): CalibrationTrial {
        let cur_trial: CalibrationTrial = this.testing_trials[this.trial_index];
    
        return cur_trial;
    }

    getBGMaskDataBool(x: number, y: number): number {
        let ind: number = (y * 400) + x;
        return this.background_masks_bool[this.mask_index][ind];
    }

    getBGMaskData(x: number, y: number): number {
        // console.log(`${this.mask_index} ${x} ${y}`);
        // console.log(this.background_masks[this.mask_index]);

        let ind: number = ((y * this.background_masks[this.mask_index].width) + x) * 4;
        let rgb = ColourConverterStandalone.packageIntRGB(this.background_masks[this.mask_index].data[ind], this.background_masks[this.mask_index].data[ind+1], this.background_masks[this.mask_index].data[ind+2])
        
        // console.log(`y(${y}) * width(${this.background_masks[this.mask_index].width}) + x(${x}) = ind(${ind}) -> ${this.background_masks[this.mask_index].data[ind]}`);
        // console.log(`ind:${ind} -> ${rgb}`);
        return rgb;
    }

    getMaskIndex(): number {
        return this.mask_index;
    }

    incrementSkipCount(): void {
        this.skip_count++;
    }

    incrementCorrectCount(): void {
        this.correct_count++;
    }

    incrementScreenCount(): void {
        this.screen_count++;
    }

    finishATrial(cur_trial: CalibrationTrial): void {
        this.finished_trials.push(cur_trial);
        this.testing_trials.splice(this.trial_index, 1);

        if (this.testing_trials.length == 0) {
            let test_data: any[][] = [];
            let total_time: Number = new Date().getTime() - this.start_time - this.menu_time;
            let uuid: string = this.UUID;
            let env_id: string = this.env_id;
            let age: string = this.age;
            let sex: string = this.sex;
            // console.log(`UUID: TODO\tTotal Trials: ${this.screen_count}\tCorrect: ${this.correct_count}\tSkipped: ${this.skip_count}\tTotal Time: ${total_time}`);
            // console.log("TYPE\tB_RED\tB_GREEN\tB_BLUE\tMIN_R\tMIN_G\tMIN_B\tMAX_R\tMAX_G\tMAX_B\tB_L\tB_U\tB_V\tMIN_L\tMIN_U\tMIN_V\tMAX_L\tMAX_U\tMAX_V\tDISTANCE");
            for (let printCT of this.finished_trials) {
                let ct_str: string = printCT.toString();
                let ct_parts: any[] = ct_str.split('\t');

                let cur_data = [uuid, env_id, age, sex, this.screen_count, this.correct_count, this.skip_count, total_time, 
                    printCT.getTotalTrialTime(), printCT.getScreenCount(), printCT.getCorrectCount(), 
                    printCT.getSkipCount(), printCT.getIndividualTimesAsString()].concat(ct_parts);
                console.log(cur_data);
                test_data.push(cur_data);
            }

            this.is_complete = true;
            this.saveData(test_data).then((resp) => {
                console.log(resp)
            });

            
        }
    }

    onToNext(): void {
        this.trial_index++;
        if (this.trial_index >= this.testing_trials.length) {
            shuffle(this.testing_trials);
            this.trial_index = 0;
        }

        this.time_record = Date.now();
    }

    async saveData(data: any[][]) {
        const response = await fetch('/api/save_ctest_results', {
            method: 'POST', // *GET, POST, PUT, DELETE, etc.
            mode: 'cors', // no-cors, *cors, same-origin
            credentials: 'same-origin', // include, *same-origin, omit
            headers: {
              'Content-Type': 'application/json'
              // 'Content-Type': 'application/x-www-form-urlencoded',
            },
            redirect: 'follow', // manual, *follow, error
            referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
            body: JSON.stringify(data) // body data type must match "Content-Type" header
        });
        return response.json()
    }

    async loadConfusionLineColors(): Promise<number[][]> {
        // Define return value to size NUM_CONFUSION_LINE_COLORS
        // This defines AND initializes all values to null
        let confusion_line_colors: number[][] = Array.apply(null, Array(CalibrationConstants.NUM_CONFUSION_LINES)).map(function () {});
    
        const file_contents = await fetch(CalibrationConstants.RESOURCE_PATH + 'colorsNoNoise.txt').then(response => response.text());
        const lines = file_contents.split('\n');
    
        for(let i=0; i < CalibrationConstants.NUM_CONFUSION_LINES; i++) {
            const line_split = lines[i].split('\t');
            let line_colors: number[] = Array.apply(null, Array(+line_split[0])).map(function (){});
    
            for(let j = 0; j < +line_split[0]; j++) {
                line_colors[j] = +line_split[j+1];
            }
    
            confusion_line_colors[i] = line_colors;
        }
        return confusion_line_colors;
    }

    async generateCalibrationTrials(): Promise<void> {
        let confusion_line_colors: number[][] = await this.loadConfusionLineColors();
    
        let bases: any[] = [
            { c: ColourConverterStandalone.packageIntRGB(17, 17, 17), dir: [CalibrationType.LUMINANCE_UP] }, // LUM UP ONLY 
            { c: ColourConverterStandalone.packageIntRGB(48, 48, 48), dir: [CalibrationType.LUMINANCE_UP, CalibrationType.LUMINANCE_DOWN] }, 
            { c: ColourConverterStandalone.packageIntRGB(82, 82, 82), dir: [CalibrationType.LUMINANCE_UP, CalibrationType.LUMINANCE_DOWN] }, 
            { c: ColourConverterStandalone.packageIntRGB(119, 119, 119), dir: [CalibrationType.LUMINANCE_UP, CalibrationType.LUMINANCE_DOWN] }, 
            { c: ColourConverterStandalone.packageIntRGB(158, 158, 158), dir: [CalibrationType.LUMINANCE_UP, CalibrationType.LUMINANCE_DOWN] },
            { c: ColourConverterStandalone.packageIntRGB(198, 198, 198), dir: [CalibrationType.LUMINANCE_UP, CalibrationType.LUMINANCE_DOWN] },
            { c: ColourConverterStandalone.packageIntRGB(241, 241, 241), dir: [CalibrationType.LUMINANCE_DOWN] }, // LUM DOWN ONLY
        ];

        let study3bases: any[] = [
            
            //points along L*
            { c: ColourConverterStandalone.packageIntRGB(17, 17, 17), dir: [CalibrationType.LUMINANCE_UP] }, // LUM UP ONLY 
            { c: ColourConverterStandalone.packageIntRGB(48, 48, 48), dir: [CalibrationType.LUMINANCE_UP, CalibrationType.LUMINANCE_DOWN] }, 
            { c: ColourConverterStandalone.packageIntRGB(82, 82, 82), dir: [CalibrationType.LUMINANCE_UP, CalibrationType.LUMINANCE_DOWN] }, 
            { c: ColourConverterStandalone.packageIntRGB(119, 119, 119), dir: [CalibrationType.LUMINANCE_UP, CalibrationType.LUMINANCE_DOWN] }, 
            { c: ColourConverterStandalone.packageIntRGB(158, 158, 158), dir: [CalibrationType.LUMINANCE_UP, CalibrationType.LUMINANCE_DOWN] },
            { c: ColourConverterStandalone.packageIntRGB(198, 198, 198), dir: [CalibrationType.LUMINANCE_UP, CalibrationType.LUMINANCE_DOWN] },
            { c: ColourConverterStandalone.packageIntRGB(241, 241, 241), dir: [CalibrationType.LUMINANCE_DOWN] }, // LUM DOWN ONLY

            /* 
                points along "R" primary line
                1/3:  LUV: 53.241, 57.755, 12.495,  RGB: 186,104,104
                +15: LUV: 68.241, 57.755, 12.495,   RGB: 228,143,143
                -15: LUV: 38.241, 57.755, 12.495,   RGB: 146,65,65
                2/3:  LUV: 53.241, 115.51, 24.919   RGB: 225,73,73
            */

            /*
                points along "G" primary line
                1/3: LUV: 87.735, -27.416, 35.442, RGB: 179,234,179
                -15: LUV: 72.735, -27.416, 35.442, RGB: 137,192,137
                -30: LUV: 57.735, -27.416, 35.442, RGB: 96,152,96
                2/3: LUV: 87.735, -54.831, 70.883, RGB: 128,245,128
            */

            /*
                points along "B" primary line
                1/3: LUV: 32.297, -3.135, -43.447,  RGB: 70,70,125
                +15: LUV: 47.297, -3.135, -43.447,  RGB: 107,107,160
                -15: LUV: 57.297, -3.135, -43.447,  RGB: 133,133,185
                2/3: LUV: 32.297, -6.27, -86.894    RGB: 59,59,178
            */
        ];

        console.log(bases);
        for(let i=0; i<bases.length; i++) {
            for(let j=0; j<this.num_repetitions; j++) { // Hardcoded 2 num_repititions
                // Confusion line trials
                // this.testing_trials.push(new CalibrationTrial(CalibrationType.PROTAN_TOWARD, bases[i], confusion_line_colors));
                // this.testing_trials.push(new CalibrationTrial(CalibrationType.PROTAN_AWAY, bases[i], confusion_line_colors));
                // this.testing_trials.push(new CalibrationTrial(CalibrationType.DEUTAN_TOWARD, bases[i], confusion_line_colors));
                // this.testing_trials.push(new CalibrationTrial(CalibrationType.DEUTAN_AWAY, bases[i], confusion_line_colors));
                // this.testing_trials.push(new CalibrationTrial(CalibrationType.TRITAN_TOWARD, bases[i], confusion_line_colors));
                // this.testing_trials.push(new CalibrationTrial(CalibrationType.TRITAN_AWAY, bases[i], confusion_line_colors));
                for (let d of bases[i].dir) {
                    this.testing_trials.push(new CalibrationTrial(d, bases[i].c, confusion_line_colors));
                }
                // Luminance trials
                
                // this.testing_trials.push(new CalibrationTrial(CalibrationType.LUMINANCE_DOWN, bases[i].c, confusion_line_colors));
            }
        }
    
        shuffle(this.testing_trials);
    
        this.estimated_screen_count = 8.0 * this.testing_trials.length;
    }

    pauseProgression() {
        this.progressing = false;
    }
}

function shuffle(a) {
    for (let i = a.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [a[i], a[j]] = [a[j], a[i]];
    }
    return a;
}

(window as any).CTest = CTest;

export default { CTest };

