
class CalibrationConstants {
    static WAIT_TIME = 500;
    static SIZE = 4;
    static GAP = 4;
    static RLM_VALUES = [ -5.0, -4.0, -3.0, -2.0, -1.0, 0.0, 1.0, 2.0, 3.0, 4.0, 5.0 ];
    static NUM_CONFUSION_LINES = 6;

    static MASKS = {
        TT: 0,
        TR: 1,
        RR: 2,
        BR: 3,
        BB: 4,
        BL: 5,
        LL: 6,
        TL: 7
    };

    static RESOURCE_PATH = "ctest/resources/";

}

export { CalibrationConstants };