$(document).ready( function () {
    const focus_ids = [
        '#uuid_input',
        '#env_input',
        '#age_input',
        '#sex_input',
    ];
    let focus_ind = 0;
    $(focus_ids[focus_ind]).focus();
    let has_started = false;
    let keys = {};

    $(document).keydown(function (e) {
        keys[e.which] = true;

        // Enter and hasn't started
        if (keys[13] && !has_started) {
            has_started = start_btn();
        }
        // / (on num pad) to emulate tab and move to next input field
        else if(keys[111]) {
            e.preventDefault();
            focus_ind = (focus_ind < focus_ids.length - 1) ? focus_ind + 1 : 0;
            $(focus_ids[focus_ind]).focus();
        }

        // +/-/. on numpad
        if (keys[109] && keys[110] && keys[107]) {
            window.location.reload();
        }


    });

    $(document).keyup(function (e) {
        delete keys[e.which];
    });

    // $(document).keypress(function (e) {
    //     let key = e.which;
    //     if(key == 13)  // the enter key code
    //     {
    //         start_btn(); 
    //     }
    // })
    $('#start_btn').click(start_btn);
});

function is_ready_to_start() {
    const uuid = $("#uuid_input").val();
    const env  = $("#env_input").val();
    const age  = $("#age_input").val();
    const sex  = $("#sex_input").val();

    // Must have uuid and env id
    if (uuid == "" || env == "") {
        return false;
    }

    // if age or sex is empty, it can't be environment 0
    if (age == "" || sex == "") {
        if (env == "0") {
            return false
        }
    }

    // sex must be 1 (male), 2 (female), 3 (not disclosed)
    if (sex != "") {
        if (sex != "1" && sex != "2" && sex != "3") {
            return false;
        }
    }

    return true;
}

function start_btn () {
    if (is_ready_to_start()) {
        $('#ctest_root').css('visibility', 'visible');
        $('#instruction_div').css('visibility', 'hidden');
        return true;
    }
    return false;
}