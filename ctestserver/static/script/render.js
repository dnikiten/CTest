let ct = null;
const VALID_KEYS = [97, 98, 99, 100, 101, 102, 103, 104, 105, 32];
const WAIT_TIME = 400;
// This should be removed from CalibrationConstants and renamed
const SIZE = 4; 
const GAP = 4;
const MASKS = {
    TT: 0,
    TR: 1,
    RR: 2,
    BR: 3,
    BB: 4,
    BL: 5,
    LL: 6,
    TL: 7
};

let MASKS_LOADED = false;
let TEST_STARTED = false;

let timer_started = false
let total_time = 0
let trial_displayed_time = 0
let user_pressed_time = 0

function calculateTrialTime(){
    return (user_pressed_time - trial_displayed_time) / 1000
}

// function logTest() {
//     console.log("Being called from " + arguments.callee.caller.toString());
// }

// P5 calls setup function with file included (render.js)
async function setup() {
    // Create new CTest object -> pass in 3 -- ...
    ct = new window.CTest(3);
    await ct.setup();


    createCanvas(ct.settings.size[0], ct.settings.size[1]);
    ellipseMode(CORNER);
    frameRate(25);
    MASKS_LOADED = true;
}

// P5 calls draw function with file included (render.js)
function draw() {
    background(0);
    
    if (!TEST_STARTED) {
        testStarted();
        return;
    }
    
    if (ct.isProgressing() && !ct.isComplete() && MASKS_LOADED) {
        const noise = ct.getCurrentTrialNoise();
        let bg_noise = noise[0];
        let fg_noise = noise[1];
        let index;

        // console.log(bg_noise);
        // console.log(fg_noise);

        for (let y=0; y<ct.settings.size[1]; y=y+SIZE + GAP) {
            for (let x=0; x<ct.settings.size[0]; x=x+SIZE + GAP) {
                // let bg_mask_data = ct.getBGMaskData(x, y);
                let bg_mask_data = ct.getBGMaskDataBool(x, y);
                // if (bg_mask_data.toString(16) != 'FFFFFF') {
                if (bg_mask_data == 0) {
                    index = Math.floor(Math.random() * fg_noise.length);
                    let c = color(fg_noise[index][0], fg_noise[index][1], fg_noise[index][2]);
                    // let colour_str = fg_noise[index].toString(16).replace('-', '#');
                    // console.log(fg_noise[index]);
                    // let colour_str = "#000000"
                    // if (colour_str.length != 7) {
                    //     console.log("fg", colour_str, fg_noise[index], fg_noise[index].toString(16));
                    // }
                    stroke(c);
                    fill(c);
                }
                else {
                    index = Math.floor(Math.random() * bg_noise.length);
                    let c = color(bg_noise[index][0], bg_noise[index][1], bg_noise[index][2]);
                    // let colour_str = bg_noise[index].toString(16).replace('-', '#');
                    
                    // let colour_str = "#FFFFFF"
                    // if (colour_str.length != 7) {
                    //     console.log("bg", colour_str, bg_noise[index], bg_noise[index].toString(16));
                    // }
                    // console.log(bg_noise[index])
                    stroke(c);
                    fill(c);
                }

                ellipse(x, y, SIZE, SIZE);
            }
        }
    }
    else {
        if(ct.isComplete()) {
            let s = 'Trial Complete!';
            textSize(30);
            fill(255, 255, 255);
            text(s, 20, 20, 400, 400); // Text wraps within text box
        }
        else {
            // Display progress bar
            let current_time = Date.now();
            // Due to this comparison, the total_time isn't completely accurate
            if (ct.time_record + WAIT_TIME <= current_time) {
                ct.nextTrial(current_time);
                // Start timer on display of stimuli -- only start the time once
                if (!timer_started){
                    timer_started = true;
                    trial_displayed_time = performance.now()
                }
            }
        }
        
    }

    // Instructions and end screen?

}

function mouseClicked() {
    let EDGE0 = 0;
    let STEP1 = 133;
    let STEP2 = 266;
    let EDGE4 = 400;

    if (mouseX > EDGE0 && mouseX < STEP1 && mouseY > EDGE0 && mouseY < STEP1) { keyPressed(103); }	// top-left
    if (mouseX > STEP1 && mouseX < STEP2 && mouseY > EDGE0 && mouseY < STEP1) { keyPressed(104); }	// top-centre
    if (mouseX > STEP2 && mouseX < EDGE4 && mouseY > EDGE0 && mouseY < STEP1) { keyPressed(105); }	// top-right

    if (mouseX > EDGE0 && mouseX < STEP1 && mouseY > STEP1 && mouseY < STEP2) { keyPressed(100); }	// middle-left
    if (mouseX > STEP1 && mouseX < STEP2 && mouseY > STEP1 && mouseY < STEP2) { keyPressed(32); }	// middle-centre
    if (mouseX > STEP2 && mouseX < EDGE4 && mouseY > STEP1 && mouseY < STEP2) { keyPressed(102); }	// middle-right

    if (mouseX > EDGE0 && mouseX < STEP1 && mouseY > STEP2 && mouseY < EDGE4) { keyPressed(97); }	// bottom-left
    if (mouseX > STEP1 && mouseX < STEP2 && mouseY > STEP2 && mouseY < EDGE4) { keyPressed(98); }	// bottom-centre
    if (mouseX > STEP2 && mouseX < EDGE4 && mouseY > STEP2 && mouseY < EDGE4) { keyPressed(99); }	// bottom-right
}

function keyPressed(code) {
    let key_code = (typeof code === 'object' && 'keyCode' in code) ? code.keyCode : code;
    
    if (ct.isProgressing() && VALID_KEYS.includes(key_code)) {
        // Record time
        user_pressed_time = performance.now()
        const trial_time = calculateTrialTime()
        // total_time just for testing
        total_time += trial_time
        
        ct.incrementScreenCount();
        let cur_trial = ct.getCurrentTrial();
        // Adjust values based on the current calibration trial, add the individual time to string containing all times
        cur_trial.incrementScreenCount();
        cur_trial.trial_times.push(trial_time)
        console.log(cur_trial.toString());
        
        let mask_ind  = ct.getMaskIndex();
        // console.log(mask_ind);
        switch(key_code) {
            case (32): // SPACE
            case (101):
                cur_trial.recordIsDifferentiable(false);
                ct.incrementSkipCount();
                cur_trial.incrementSkipCount();
                break;
            case (97): // BL
                cur_trial.recordIsDifferentiable(mask_ind == MASKS.BL);
                if (mask_ind == MASKS.BL){
                    ct.incrementCorrectCount();
                    cur_trial.incrementCorrectCount();
                } 
                break;
            case (98): // BB
                cur_trial.recordIsDifferentiable(mask_ind == MASKS.BB);
                if (mask_ind == MASKS.BB){
                    ct.incrementCorrectCount();
                    cur_trial.incrementCorrectCount();
                } 
                break;
            case (99): // BR
                cur_trial.recordIsDifferentiable(mask_ind == MASKS.BR);
                if (mask_ind == MASKS.BR){
                    ct.incrementCorrectCount();
                    cur_trial.incrementCorrectCount();
                } 
                break;
            case (100): // LL
                cur_trial.recordIsDifferentiable(mask_ind == MASKS.LL);
                if (mask_ind == MASKS.LL){
                    ct.incrementCorrectCount();
                    cur_trial.incrementCorrectCount();
                } 
                break; 
            case (102): // RR
                cur_trial.recordIsDifferentiable(mask_ind == MASKS.RR);
                if (mask_ind == MASKS.RR){
                    ct.incrementCorrectCount();
                    cur_trial.incrementCorrectCount();
                } 
                break;
            case (103): // TL
                cur_trial.recordIsDifferentiable(mask_ind == MASKS.TL);
                if (mask_ind == MASKS.TL){
                    ct.incrementCorrectCount();
                    cur_trial.incrementCorrectCount();
                } 
                break; 
            case (104): // TT
                cur_trial.recordIsDifferentiable(mask_ind == MASKS.TT);
                if (mask_ind == MASKS.TT){
                    ct.incrementCorrectCount();
                    cur_trial.incrementCorrectCount();
                } 
                break;
            case (105): // TR
                cur_trial.recordIsDifferentiable(mask_ind == MASKS.TR);
                if (mask_ind == MASKS.TR){
                    ct.incrementCorrectCount();
                    cur_trial.incrementCorrectCount();
                } 
                break;
        }

        // sets progressing to false
        ct.pauseProgression();
        timer_started = false;
        if (cur_trial.isFinished()) {
            ct.finishATrial(cur_trial);
        }
        ct.onToNext();
    }
}


function testStarted() {
    // Messy but works
    let vis = $('#ctest_root').css('visibility');
    if (vis == 'hidden') {
        return false;
    }
    else if(vis == 'visible') {
        const uuid = $("#uuid_input").val();
        const env  = $("#env_input").val();
        const age  = $("#age_input").val();
        const sex  = $("#sex_input").val();
        TEST_STARTED = true;
        ct.menu_time = new Date().getTime() - ct.start_time;
        ct.UUID      = uuid
        ct.env_id    = env
        ct.age       = (age != "") ? age : "0" 
        ct.sex       = (sex != "") ? sex : "0"
        ct.progressing = false;
    }
    // console.log(vis);
}
