import java.awt.geom.Line2D;
import java.util.ArrayList;
import java.util.Arrays;

public class Main {
    // from Denis
    // R: (27, 57.755, 12.459) - (77, 57.755, 12.459)
    // G: (45, -27.416, 35.442) - (95, -27.416, 35.442)
    // B: (11, -3.135, -43.447) - (61, -3.135, -43.447)

    public static void main(String[] args) {
        // double[] luv1 = ColourConverterStandAlone.RGBtoLUV(0x00000000);
        // double[] luv2 = ColourConverterStandAlone.RGBtoLUV(0x00FFFFFF);

        // Denis reds
//        double[] luv1 = { 27.0, 57.755, 12.459 };
//        double[] luv2 = { 77.0, 57.755, 12.459 };

        // Denis greens
//        double[] luv1 = { 45.0, -27.416, 35.442 };
//        double[] luv2 = { 95.0, -27.416, 35.442 };

        // Denis blues
        double[] luv1 = { 11.0, -3.135, -43.447 };
        double[] luv2 = { 61.0, -3.135, -43.447 };

//        Colour[] colours = getRGBsEpsilon(luv1, luv2, 0.3);
    /*
        Colour[] colours = getRGBsStep(luv1, luv2);
        System.out.println("Number: " + colours.length);

        for (Colour c : colours) {
            System.out.println(c.toString());
        }
    */
        double[] testLuv = {68.241, 57.755, 12.495}; 
        int[] testRGB = ColourConverterStandAlone.unpackageIntRGB(ColourConverterStandAlone.LUVtoRGB(testLuv));
        System.out.printf("Colour %d %d %d\n", testRGB[0],testRGB[1],testRGB[2]);
    }

    public static final double DELTA = 0.1;

    /* For a given range of luminance values starting at min(LUV1[0],LUV2[0]), and
     * increasing by 'step' until max(LUV1[0],LUV2[0]) is reached, determine the
     * closed sRGB colour to the line between LUV1 to LUV2 for each luminance value.
     * Returns a luminance-sorted array of Colours. Assumes that the two CIELuv points
     * provided only differ in luminance (~same u* and v*). */
    public static Colour[] getRGBsStep(double[] LUV1, double[] LUV2) {
        assert(LUV1.length == 3 && LUV2.length == 3);

        // check if isoluminant 
//        double lDiff = Math.abs(LUV1[0] - LUV2[0]);
        double uDiff = Math.abs(LUV1[1] - LUV2[1]);
        double vDiff = Math.abs(LUV1[2] - LUV2[2]);

        // isoluminant case
/*        if (lDiff < DELTA) {
            double epSq = epsilon * epsilon;
            double lAvg = (LUV1[0] + LUV2[0])/2.0;
            Line2D.Double line = new Line2D.Double(LUV1[1], LUV1[2], LUV2[1], LUV2[2]);
            ArrayList<Colour> closeInts = new ArrayList<>();
            for (int rgb = 0; rgb <= 0x00FFFFFF; rgb = rgb + 1) {
                Colour c = new Colour(rgb);
                if (Math.abs(c.L - lAvg) <= epsilon && line.ptLineDistSq(c.U, c.V) <= epSq) {
                    closeInts.add(c);
                }
            }
            Colour[] tempColours = new Colour[0];
            return closeInts.toArray(tempColours);
        }
*/
        // iso u + v case (isochroma + isohue)
        if (uDiff < DELTA && vDiff < DELTA) {
            double step = 1.0;
            double uAvg = (LUV1[1] + LUV2[1]) / 2.0;
            double vAvg = (LUV1[2] + LUV2[2]) / 2.0;

            long minLum_x10 = Math.round(10 * Math.min(LUV1[0], LUV2[0]));
            long maxLum_x10 = Math.round(10 * Math.max(LUV1[0], LUV2[0]));
            int count = 0;
            for (double l = minLum_x10; l <= maxLum_x10; l=l+step) {
                count = count + 1;
            }
            System.out.println("count = " + count);

            Colour[] colours = new Colour[count];
            Arrays.fill(colours, null);

            for (int rgb=0; rgb<=0x00FFFFFF; rgb=rgb+1) {
                Colour c = new Colour(rgb);
                long lum_x10 = Math.round(10 * c.L);
                if (lum_x10 >= minLum_x10 && lum_x10 <= maxLum_x10) {   // within luminance bounds
                    int index = (int) (lum_x10 - minLum_x10);
                    if (colours[index] == null) {
                        colours[index] = c;
                    }
                    else { // not null, so we've found a previous colour for this lum level
                        double dist_c = ColourConverterStandAlone.findDistance(c.U, c.V, uAvg, vAvg);
                        double dist_prev = ColourConverterStandAlone.findDistance(colours[index].U, colours[index].V, uAvg, vAvg);
                        if (dist_c <= dist_prev) {
                            colours[index] = c;
                        }
                    }
                }
            }
            return colours;
        }
        else { // error condition - line is not isoluminant, isohue, or isochrom
            System.err.println("ERROR: points must be isoluminant or only different by luminance");
            return null;
        }
    }


    // ABANDONED!
    /* Determines a line in CIELuv space between the two CIELuv points provided.
     * Returns an (unorded) array of Colours, such that every Colour is
     * within 'epsilon' of that line. Assumes that the two CIELuv points provided
     * are either isolumninant (~same L*) or only differ in luminance (~same u* and v*). */
    public static Colour[] getRGBsEpsilon(double[] LUV1, double[] LUV2, double epsilon) {
        assert(LUV1.length == 3 && LUV2.length == 3);

        // check if isoluminant 
        double lDiff = Math.abs(LUV1[0] - LUV2[0]);
        double uDiff = Math.abs(LUV1[1] - LUV2[1]);
        double vDiff = Math.abs(LUV1[2] - LUV2[2]);

        // isoluminant case
        if (lDiff < DELTA) {
            double epSq = epsilon * epsilon;
            double lAvg = (LUV1[0] + LUV2[0])/2.0;
            Line2D.Double line = new Line2D.Double(LUV1[1], LUV1[2], LUV2[1], LUV2[2]);
            ArrayList<Colour> closeInts = new ArrayList<>();
            for (int rgb = 0; rgb <= 0x00FFFFFF; rgb = rgb + 1) {
                Colour c = new Colour(rgb);
                if (Math.abs(c.L - lAvg) <= epsilon && line.ptLineDistSq(c.U, c.V) <= epSq) {
                    closeInts.add(c);
                }
            }
            Colour[] tempColours = new Colour[0];
            return closeInts.toArray(tempColours);
        }

        // iso u + v case (isochroma + isohue)
        else if (uDiff < DELTA && vDiff < DELTA) {
            double uAvg = (LUV1[1] + LUV2[1]) / 2.0;
            double vAvg = (LUV1[2] + LUV2[2]) / 2.0;
            ArrayList<Colour> closeInts = new ArrayList<>();
            for (int rgb = 0; rgb <= 0x00FFFFFF; rgb = rgb + 1) {
                Colour c = new Colour(rgb);
                if (Math.abs(c.U - uAvg) <= epsilon && Math.abs(c.V - vAvg) <= epsilon) {
                    closeInts.add(c);
                }
            }
            Colour[] tempColours = new Colour[0];
            return closeInts.toArray(tempColours);
        }
        else { // error condition - line is not isoluminant, isohue, or isochrom
            System.err.println("ERROR: points must be isoluminant or only different by luminance");
            return null;
        }
    }
}
