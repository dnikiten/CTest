public class Colour {
    public final int RGB;
    public final int[] RGBArray;
    public final int R;
    public final int G;
    public final int B;
    public final double[] LUV;
    public final double L;
    public final double U;
    public final double V;

    public Colour(int rgb) {
        assert(rgb >= 0 && rgb <= 0x00FFFFFF);

        this.RGB = rgb;
        this.RGBArray = ColourConverterStandAlone.unpackageIntRGB(rgb);
        this.R = RGBArray[0];
        this.G = RGBArray[1];
        this.B = RGBArray[2];

        this.LUV = ColourConverterStandAlone.RGBtoLUV(rgb);
        this.L = LUV[0];
        this.U = LUV[1];
        this.V = LUV[2];
    }

    public Colour(int r, int g, int b) {
        this(ColourConverterStandAlone.packageIntRGB(r, g, b));
    }

    public Colour(double[] luv) {
        assert(luv[0] >= ColourConverterStandAlone.MIN_LUM_LUV && luv[0] <= ColourConverterStandAlone.MAX_LUM_LUV);
        assert(luv[1] >= ColourConverterStandAlone.MIN_U_LUV && luv[1] <= ColourConverterStandAlone.MAX_U_LUV);
        assert(luv[2] >= ColourConverterStandAlone.MIN_V_LUV && luv[2] <= ColourConverterStandAlone.MAX_V_LUV);

        this.LUV = luv;
        this.L = luv[0];
        this.U = luv[1];
        this.V = luv[2];

        this.RGB = ColourConverterStandAlone.LUVtoRGB(luv);
        this.RGBArray = ColourConverterStandAlone.unpackageIntRGB(this.RGB);
        this.R = RGBArray[0];
        this.G = RGBArray[1];
        this.B = RGBArray[2];
    }

//    public Colour(double l, double u, double v) {
//        double[] tempLUV = { l, u, v };
//        this(tempLUV);
//    }

    @Override
    public String toString() {
        return "" + R + "\t" + G + "\t" + B + "\t" + L + "\t" + U + "\t" + V;
    }
}
